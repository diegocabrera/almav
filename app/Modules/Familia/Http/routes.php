<?php

Route::group(['middleware' => 'web', 'prefix' => 'familia', 'namespace' => 'App\\Modules\Familia\Http\Controllers'], function()
{
    Route::get('/', 'FamiliaController@index');
});
