<?php

namespace App\Modules\Multimedia\Http\Controllers;

//Controlador Padre
use App\Modules\Multimedia\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Validator;
use Image;

//Request
use App\Modules\Multimedia\Http\Requests\GaleriaRequest;

//Modelos
use App\Modules\Multimedia\Models\Galeria;
use App\Modules\Multimedia\Models\GaleriaImagenes;

class GaleriaController extends Controller
{
    public $js = ['Galeria'];
    public $librerias = [
        'alphanum',
        'maskedinput',
        'datatables',
        'jquery-ui',
        'jquery-ui-timepicker',
        'file-upload',
        'jcrop'
    ];

    public $perfiles_publicar = [1];

    public function index(){
        return $this->view('multimedia::Galeria', [
            'Galeria'=> new Galeria()
        ]);
    }

    public function buscar(Request $request, $id =0){
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta().'/destruir')) {
            $rs = Galeria::withTrashed()->find($id);
        }else {
            $rs = Galeria::find($id);
        }
        $url = $this->ruta();
        $url=substr($url,0,strlen($url)-7);
        if ($rs) {
            $imgArray=[];
            $imgs = GaleriaImagenes::where('galeria_id', $id)->get();
            foreach ($imgs as $img) {
                $id_archivo=str_replace ('/','-', $img->archivo);
                $name=substr($id_archivo, strrpos($id_archivo,'/')+1);
                $imArray[]=[
                    'id'=>$id_archivo,
                    'name'=>$name,
                    'url'=>url('public/archivos/galeria/'.$img->archivo),
                    'thumbnailUrl'=>url('public/archivos/galeria/'.$img->archivo),
                    'deleteType'=>'DELETE',
                    'deleteUrl'=>url($url.'/eliminarimagen'.$id_archivo),
                    'data'=>[
                        'cordenadas'=>[],
                        'leyenda'=>$img->leyenda,
                        'descripcion'=>$img->descripcion
                    ]
                ];
            }
            $respuesta = array_merge($rs->toArray(),[
                's'=>'s',
                'msj'=>trans('controller.buscar'),
                'files'=>$imArray
            ]);
            return $respuesta;
        }
        return trans('controller.nobuscar');
    }

    public function data($request){
        if ($this->puedePublicar() && $request['published_at'] != '') {
            $data=$request;
        }else {
            $data=$request->except(['published_at']);
        }
        return $data;
    }

    public function guardar(GaleriaRequest $request,$id=0){
       DB::beginTransaction();
       try {

           $data = $this->data($request->all());
           $archivos = json_decode($request->archivos);

           if (empty($archivos)) {
               unset($data['published_at']);
           }

           if ($id === 0) {
               $Galeria = Galeria::create($data);
               $id = $Galeria->id;
           }else {
               if (empty($archivos)) {
                   unset($data['published_at']);
               }
               $Galeria = Galeria::find($id)->update($data);
           }

           $this->guardarImagenes($archivos, $id);
           //$this->procesar_permisos($request, $id);
       } catch (QueryException $e) {
           DB::rollback();
           return $e->getMessage();
       } catch (Exception $e) {
           DB::rollback();
           return $e->errorInfo[2];
       }
       DB::commit();
       return [
           'id' => $Galeria->id,
           'texto' => $Galeria->titulo,
           's' => 's',
           'msj' => trans('controller.incluir')
       ];

    }

    protected function getRuta() {
        return date('Y') . '/' . date('m') . '/';
    }

    protected function guardarImagenes($archivos,$id){
        foreach ($archivos as $archivo => $data) {
            if (!preg_match("/^(\d{4})\-(\d{2})\-([0-9a-z\.]+)\.(jpe?g|png)$/i", $archivo)) {
                continue;
            }
            $archivo = str_replace('-','/',$archivo);
            $imagen = Image::make(public_path('archivos/galeria/' . $archivo));
            $imagenes = GaleriaImagenes::firstOrNew(array('archivo'=>$archivo));
            $imagenes->fill([
                'galeria_id'=>$id,
                'tamano'=>$imagen->width().'x'.$imagen->height(),
                'leyenda'=>$data->leyenda,
                'descripcion'=>$data->descripcion
            ]);
            $imagenes->save();
        }
    }

    public function eliminar(Request $request, $id=0){
        try{
            $rs=Galeria::destroy($id);
        }catch (Exception $e){
            return $e->errorInfo[2];
        }
        return ['s'=>'s', 'msj'=>trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id=0){
        try {
            Galeria::withTrashed()->find($id)->restore();
        }catch(Exception $e){
            return $e->errorInfo[2];
        }
        return ['s'=>'s', 'msj'=>trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id=0){
        try {
			Galeria::withTrashed()->find($id)->forceDelete();
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}
		return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function eliminarImagen(Request $request, $id=0){
        $id = str_replace('-', '/', $id);
		try {
			// \File::delete(public_path('img/noticias/' . $id));
			$rs = GaleriaImagenes::where('archivo', $id)->delete();
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}
		return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function subir(Request $request) {
        $validator=Validator::make($request->all(),[
            'files.*' => [
                'required',
                'mimes:jpeg,jpg,png'
            ],
        ]);
        if ($validator->fails()) {
            return 'Error de Validación';
        }
        $files = $request->file('files');
        $url = $this->ruta();
        $url = substr($url, 0, strlen($url) - 6);
        $rutaFecha = $this->getRuta();
        $ruta = public_path('archivos/galeria/' . $rutaFecha);
        $respuesta = array(
            'files' => array(),
        );
        foreach ($files as $file) {
            do {
                $nombre_archivo = $this->random_string() . '.' . $file->getClientOriginalExtension();
            } while (is_file($ruta . $nombre_archivo));
            $id = str_replace('/', '-', $rutaFecha . $nombre_archivo);
            $respuesta['files'][] = [
                'id' => $id,
                'name' => $nombre_archivo,
                'size' => $file->getSize(),
                'type' => $file->getMimeType(),
                //'url' => url('imagen/small/' . $rutaFecha . $nombre_archivo),
                'url' => url('public/archivos/galeria/' . $rutaFecha . $nombre_archivo),
                'thumbnailUrl' => url('public/archivos/galeria/' . $rutaFecha . $nombre_archivo),
                'deleteType' => 'DELETE',
                'deleteUrl' => url($url . '/eliminarimagen/' . $id),
                'data' => [
                    'cordenadas' => [],
                    'leyenda' => '',
                    'descripcion' => ''
                ]
            ];

            $mover = $file->move($ruta, $nombre_archivo);
        }
        return $respuesta;
    }

    protected function random_string($length = 5) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $key;
    }

    public function puedePublicar(){
        return strtolower(auth()->user()->super) === 's' || $this->permisologia('publicar');
    }

    // protected function guardarEtiquetas($request, $id) {
	// 	noticias_etiquetas::where('noticias_id', $id)->delete();
	// 	foreach ($request['etiquetas_id'] as $etiqueta) {
	// 		noticias_etiquetas::create([
	// 			'noticias_id' => $id,
	// 			'etiquetas_id' => $etiqueta,
	// 		]);
	// 	}
	// }

    public function datatable(Request $request)
    {
        $sql = Galeria::select([
            'id', 'nombre', 'published_at', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

}
