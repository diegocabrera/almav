<?php

namespace App\Modules\Multimedia\Http\Requests;

use App\Http\Requests\Request;

class GaleriaRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:200'], 
	];
}
