<?php

Route::group(['middleware' => 'web', 'prefix' => Config::get('admin.prefix').'/multimedia', 'namespace' => 'App\\Modules\Multimedia\Http\Controllers'], function()
{
   Route::group(['prefix' => 'audio'], function() {
		Route::get('/', 				'AudioController@index');
		Route::get('buscar/{id}', 		'AudioController@buscar');
        Route::post('subir',             'AudioController@subir');

		Route::post('guardar',			'AudioController@guardar');
		Route::put('guardar/{id}', 		'AudioController@guardar');

		Route::delete('eliminar/{id}', 	'AudioController@eliminar');
		Route::post('restaurar/{id}', 	'AudioController@restaurar');
		Route::delete('destruir/{id}', 	'AudioController@destruir');

		Route::get('datatable', 		'AudioController@datatable');
    });

	Route::group(['prefix' => 'video'], function() {
		Route::get('/', 				'VideoController@index');
		Route::get('buscar/{id}', 		'VideoController@buscar');

		Route::post('guardar',			'VideoController@guardar');
		Route::put('guardar/{id}', 		'VideoController@guardar');

		Route::delete('eliminar/{id}', 	'VideoController@eliminar');
		Route::post('restaurar/{id}', 	'VideoController@restaurar');
		Route::delete('destruir/{id}', 	'VideoController@destruir');

		Route::get('datatable', 		'VideoController@datatable');
    });
    Route::group(['prefix' => 'galeria'], function(){
        Route::get('/', 				'GaleriaController@index');
		Route::get('buscar/{id}', 		'GaleriaController@buscar');

		Route::post('guardar',			'GaleriaController@guardar');
		Route::put('guardar/{id}', 		'GaleriaController@guardar');

		Route::delete('eliminar/{id}', 	'GaleriaController@eliminar');
		Route::post('restaurar/{id}', 	'GaleriaController@restaurar');
        Route::delete('destruir/{id}', 	'GaleriaController@destruir');
        Route::post('subir',            'GaleriaController@subir');

		Route::get('datatable', 		'GaleriaController@datatable');
    });
});
