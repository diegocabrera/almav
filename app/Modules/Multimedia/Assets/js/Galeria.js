var aplicacion, $form, tabla, $archivo_actual = '', $archivos = {}, cordenadasImagen;
$(function () {
	aplicacion = new app('formulario', {
		'antes': function (accion) {
			$("#archivos").val(jsonToString($archivos));
			//$("#contenido_html").val(CKEDITOR.instances.contenido.getData());
		},
		'limpiar': function () {
			tabla.ajax.reload();
			//$("#contenido_html").val(CKEDITOR.instances.contenido.getData());
			$archivos = {};

			$("table tbody tr", "#fileupload").remove();

		},

		'buscar': function (r) {
			//CKEDITOR.instances.contenido.setData(r.contenido_html);
			$('#btn1').click();

			$("table tbody", "#fileupload").html(tmpl("template-download", r));
			$("table tbody .fade", "#fileupload").addClass('in');

			var archivos = r.files;
			$archivos = {};
			for (var i in archivos) {
				$archivos[archivos[i].id] = archivos[i].data;
			}
		}
	});

	$form = aplicacion.form;


	$('#eliminar').attr("disabled", true);

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [
			{ data: 'id', name: 'id' },
			{ data: 'nombre', name: 'nombre' },
			{data: 'published_at', name: 'Publicación'}

		]
	});

	$('#tabla').on("click", "tbody tr", function () {
		aplicacion.buscar(this.id);
	});

	$('#published_at', $form).datetimepicker({
		dateFormat: 'yy-mm-dd '
	});
	//var contenido = CKEDITOR.replace('contenido');
	$('#fileupload').fileupload({
		// Uncomment the following to send cross-domain cookies:
		//xhrFields: {withCredentials: true},
		url: $url + 'subir',
		disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
		maxFileSize: 999000,
		//formData: {example: 'test'},
		acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
	}).bind('fileuploaddone', function (e, data) {
		var archivo = data.result.files[0];
		$archivos[archivo.id] = archivo.data;
	});
	$('#fileupload').on('click', '.btn-info', function (evn) {
		evn.preventDefault();
		$archivo_actual = $(this).parents('tr').data('id');
		$("#editar_imagen").modal('show');

		$('#contImagen').html('<img src="' + $(this).data('url') + '" class="img-responsive" style="width: 100%;" />');

		$('img', '#contImagen').Jcrop({
			onChange: dataImagen,
			onSelect: dataImagen,
			boxWidth: 450,
			boxHeight: 400
		});

		$("#descripcion", "#editar_imagen").val($archivos[$archivo_actual].descripcion);
		$("#leyenda", "#editar_imagen").val($archivos[$archivo_actual].leyenda);


		return false;
	});

	$('#fileupload').on('click', '.btn-danger', function (evn) {
		evn.preventDefault();
		delete $archivos[$(this).parents('tr').data('id')];

	});
});

$(document).ready(function () {
	$("#nombre").keyup(function () {
		var value = slug($("#nombre").val());
		$("#slug").val(value);
	});
});
function dataImagen(cordenadas) {
	cordenadasImagen = cordenadas;
}

function stringToJson(str) {
	return $.parseJSON(str);
}

function jsonToString(json) {
	return JSON.stringify(json);
}
