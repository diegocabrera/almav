<?php
namespace App\Modules\Banner\Models;

use App\Modules\Base\Models\Modelo;

class Banner extends Modelo
{
	protected $table = 'banner';
    protected $fillable = ["categorias_id","archivo","activo"];

    protected $hidden = ["created_at","updated_at","deleted_at"];
}
