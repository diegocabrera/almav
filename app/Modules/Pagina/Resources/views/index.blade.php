@extends('pagina::layouts.master')



@section('content')



<!-- Head& -->
@include('pagina::partials.header-index')

<!-- Banner -->
@include('pagina::partials.banner')

<!-- Servicios -->
@include('pagina::partials.servicios')

<!-- Eventos -->
@include('pagina::partials.eventos')

<!-- Nosotros -->
@include('pagina::partials.about')

<!-- -->
<!-- -->
<!-- -->




<!-- Footer -->
@include('pagina::partials.footer-page')
<!-- Realizado por la dirección de Informatica y Sistemas de la Gobernación del Estado Bolívar -->


<!-- Creditos: -->

<!-- Diseño: Maria Campora "camporamaria@gmail.com" -->

<!-- Desarrollador: Alejandro Mendez "alejmendez.87@gmail.com" -->

<!-- Desarrollador: Diego Cabrera "diegocabrera123@gmail.com" -->

@stop
