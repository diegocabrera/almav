<?php $i=1; ?>
    @foreach ($noticias->get() as $noticia)
        <?php
            $imagen = clone $imagenes;
            $imagen = $imagen->where('noticias_id', $noticia->id)->first();
            if (!$imagen) {
                $imagen = new stdClass();
                $imagen->leyenda = '';
                $imagen->archivo = 'gob.png';
            }
        ?>
        @if ($i == 1 )
        @endif
        <article class="b-post b-post-5 clearfix">
            <div class="entry-media">
                    <div class="entry-media"><a class="js-zoom-images" href="{{ url('noticias/'.$noticia->slug) }}">
                        <img class="img-responsive" src="{{ url('public/archivos/noticias/'.$imagen->archivo) }}" alt="{{ $imagen->leyenda }}" width='' height="">
                    </a>

                    </div>
                    <div class="entry-main">
                        <div class="entry-header">
                            <h2 class="entry-title entry-title_spacing ui-title-inner">{{ $noticia->titulo }}</h2>
                        </div>
                        <div class="entry-content">
                            <p>{!! str_replace("\n", '<br/>', $controller->limit_text($noticia->resumen,40)) !!}</p>
                        </div>
                    </div>
            </div>
        </article>
    @endforeach
