<section class="event_part">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-md-8">
                <div class="section_tittle">
                    <h2>Upcoming Event</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="single_event media">
                    <img src="{{url('public/img/event_1.png')}}" class="align-self-center" alt="...">
                    <div class="tricker">10 Jun</div>
                    <div class="media-body align-self-center">
                        <h5 class="mt-0">Volunteeer Idea 2020</h5>
                        <p>Seed the life upon you are creat.</p>
                        <ul>
                            <li><span id="days"></span>days</li>
                            <li><span id="hours"></span>Hours</li>
                            <li><span id="minutes"></span>Minutes</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="single_event media">
                    <img src="{{url("public/img/event_2.png")}}" class="align-self-center" alt="...">
                    <div class="tricker">10 Jun</div>
                    <div class="media-body align-self-center">
                        <h5 class="mt-0">Volunteeer Idea 2020</h5>
                        <p>Seed the life upon you are creat.</p>
                        <ul>
                            <li><span id="days1"></span>days</li>
                            <li><span id="hours1"></span>Hours</li>
                            <li><span id="minutes1"></span>Minutes</li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="single_event media">
                    <img src="{{url('public/img/event_3.png')}}" class="align-self-center" alt="...">
                    <div class="tricker">10 Jun</div>
                    <div class="media-body align-self-center">
                        <h5 class="mt-0">Volunteeer Idea 2020</h5>
                        <p>Seed the life upon you are creat.</p>
                        <ul>
                            <li><span id="days2"></span>days</li>
                            <li><span id="hours2"></span>Hours</li>
                            <li><span id="minutes2"></span>Minutes</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="single_event media">
                    <img src="{{url('public/img/event_4.png')}}" class="align-self-center" alt="...">
                    <div class="tricker">10 Jun</div>
                    <div class="media-body align-self-center">
                        <h5 class="mt-0">Volunteeer Idea 2020</h5>
                        <p>Seed the life upon you are creat.</p>
                        <ul>
                            <li><span id="days3"></span>days</li>
                            <li><span id="hours3"></span>Hours</li>
                            <li><span id="minutes3"></span>Minutes</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>