<!-- Contador de Visitas -->
 @push('php')
    <?php
        function contador()
        {
            $archivo = "contador.txt"; //el archivo que contiene en numero
            $f = fopen($archivo, "r"); //abrimos el archivo en modo de lectura
            if($f)
            {
                $contador = '';
                $contador = fread($f, filesize($archivo)); //leemos el archivo
                $contador = $contador + 1; //sumamos +1 al contador
                fclose($f);
            }
            $f = fopen($archivo, "w+");
            if($f)
            {
                fwrite($f, $contador);
                fclose($f);
            }
            return $contador;
        }
    ?>
@endpush


<!--::footer_part start::-->
<footer class="footer_part">
    <div class="container">
        <div class="row justify-content-around">
            <div class="col-sm-6 col-lg-3">
                <div class="single_footer_part">
                    <img src="img/footer_logo.png" class="footer_logo" alt="">
                    <p>Heaven fruitful doesn't over lesser days appear creeping seasons so behold bearing days open
                    </p>
                    <div class="work_hours">
                        <h5>Working Hours:</h5>
                        <ul>
                            <li> <p> Monday-Friday:</p> <span> 8AM - 6PM</span></li>
                            <li> <p>Saturday-Sunday:</p> <span> 8AM - 12PM</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-2">
                <div class="single_footer_part">
                    <h4>Causes</h4>
                    <ul class="list-unstyled">
                        <li><a href="">Boat Shippment</a></li>
                        <li><a href="">Services</a></li>
                        <li><a href="">Transport Planning</a></li>
                        <li><a href="">Transportation</a></li>
                        <li><a href="">Truck Delivery Checking</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="single_footer_part footer_3">
                    <h4> our Gallery</h4>
                    <div class="footer_img">
                        <div class="single_footer_img">
                            <img src="img/footer_img/footer_img_1.png" alt="">
                        </div>
                        <div class="single_footer_img">
                            <img src="img/footer_img/footer_img_2.png" alt="">
                        </div>
                        <div class="single_footer_img">
                            <img src="img/footer_img/footer_img_3.png" alt="">
                        </div>
                        <div class="single_footer_img">
                            <img src="img/footer_img/footer_img_4.png" alt="">
                        </div>
                        <div class="single_footer_img">
                            <img src="img/footer_img/footer_img_5.png" alt="">
                        </div>
                        <div class="single_footer_img">
                            <img src="img/footer_img/footer_img_6.png" alt="">
                        </div>
                        <div class="single_footer_img">
                            <img src="img/footer_img/footer_img_7.png" alt="">
                        </div>
                        <div class="single_footer_img">
                            <img src="img/footer_img/footer_img_8.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="single_footer_part">
                    <h4>Newsletter</h4>
                    <p>Heaven fruitful doesn't over lesser in days. Appear creeping seasons deve behold bearing days
                        open
                    </p>
                    <div id="mc_embed_signup">
                        <form target="_blank"
                            action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                            method="get" class="subscribe_form relative mail_part" required>
                            <input type="email" name="email" id="newsletter-form-email" placeholder="Email Address"
                                class="placeholder hide-on-focus" onfocus="this.placeholder = ''"
                                onblur="this.placeholder = ' Email Address '" required="" type="email">
                            <button type="submit" name="submit" id="newsletter-submit"
                                class="email_icon newsletter-submit button-contactForm"><i
                                    class="far fa-paper-plane"></i></button>
                            <div class="mt-10 info"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-6">
                <div class="copyright_text">
                    <P><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></P>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="footer_icon social_icon">
                    <ul class="list-unstyled">
                        <li><a href="#" class="single_social_icon"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="single_social_icon"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#" class="single_social_icon"><i class="fas fa-globe"></i></a></li>
                        <li><a href="#" class="single_social_icon"><i class="fab fa-behance"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--::footer_part end::-->
