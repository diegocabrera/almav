<?php

namespace App\Modules\Pagina\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Modules\Pagina\Http\Controllers;
use Carbon\Carbon;
use DB;
//MODELOS//
//----NOTICIAS----//
use App\Modules\Noticias\Models\Noticias;
use App\Modules\Noticias\Models\Imagenes;
//----MULTIMEDIA----//
use App\Modules\Multimedia\Models\Video;
use App\Modules\Multimedia\Models\Galeria;
use App\Modules\Multimedia\Models\GaleriaImagenes;
use App\Modules\Audio\Models\Audio;
//----INFORMATIVO----//
use App\Modules\Informativo\Models\Debate;
use App\Modules\Informativo\Models\Efemerides;
use App\Modules\Informativo\Models\EfemeridesImg;
use App\Modules\Informativo\Models\Eventos;
use App\Modules\Informativo\Models\EventosImg;
//----Organizacion----//
use App\Modules\Organizacion\Models\organizacion;
//----RADIO----//
use App\Modules\Radio\Models\AudioProgramas;
use App\Modules\Radio\Models\Programas;
//----BANNERS----//
use App\Modules\Banner\Models\Banner;


class PaginaController extends Controller
{
    public $titulo = 'Alma Venezuela';
    public $css = [
        'bootstrap.min',
        'animate',
        'owl.carousel.min',
        'themify-icons',
        'all',
        'flaticon',
        'magnific-popup',
        'nice-select',
        'slick',
        'style'
    ];
    public $js = [
        'jquery-1.12.1.min',
        'popper.min',
        'bootstrap.min',
        'jquery.magnific-popup',
        'swiper.min',
        'wow.min',
        'jquery.smooth-scroll.min',
        'masonry.pkgd',
        'owl.carousel.min',
        'jquery.nice-select.min',
        'slick.min',
        'jquery.counterup.min',
        'waypoints.min',
        'countdown.jquery.min',
        'timer',
        'jquery.ajaxchimp.min',
        'jquery.form',
        'jquery.validate.min',
        'mail-script',
        'contact',
        'custom'
    ];
/*     public $dias = [
        "Lunes",
        "Martes",
        "Miercoles",
        "Jueves",
        "Viernes",
        "Sabado",
        "Domingo"
    ];
    public $meses = [
        1 => "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
    ];
    public $libreriasIniciales = [
		'OpenSans', 'font-awesome', 'simple-line-icons',
		'jquery-easing',
		'animate', 'bootstrap', 'bootbox',
		//'jquery-cookie'
		'pace', 'jquery-form', 'blockUI', 'jquery-shortcuts', 'pnotify', 'owl-carousel', 'wow', 'modernizr'
	];
    public $librerias = [
        'jquery-ui',
        'bootstrap',
        'jquery-slimscroll',
        'jquerybui',
        'scroll-top',
        'bootstrap-switch',
        'ziehharmonika'
    ];
 */

    public function index()
    {

        try {
            //----NOTICIAS----//
            /* $noticias = Noticias::select(
                'id',
                'titulo',
                'resumen',
                'slug',
                'published_at'
            )->where('published_at','<', Carbon::now()->format('Y-m-d H:i:s'))
            ->orderBy('published_at','desc')->take(5);

            $mes = [];
            $meses = [];
            foreach ($noticias->get() as $key => $noticia) {
                $mes[] = substr($noticia->published_at, 6, -12);
            }

            $imagenes = Imagenes::select('archivo','leyenda');

            //----INFORMATIVO----//
            $efemerides = Efemerides::select(
                'id',
                'titulo',
                'resumen',
                'slug',
                'url',
                'published_at'
            )->where('published_at','<', Carbon::now()->format('Y-m-d H:i:s'))
            ->orderBy('published_at','desc')
            ->take(4)->get();

            $eventos = Eventos::select(
                'id',
                'titulo',
                'lugar',
                'slug',
                'published_at'
            )->where('published_at','<', Carbon::now()->format('Y-m-d H:i:s'))
            ->orderBy('published_at','desc')
            ->take(3)
            ->get();

            $debate = Debate::select(
                'id',
                'titulo',
                'slug',
                'contenido',
                'resumen'
            )->where('published_at','<', Carbon::now()->format('Y-m-d H:i:s'))
            ->orderBy('published_at','desc')->first();


            //----Organizacion----//
            $organizacion = Organizacion::select(
                'mision',
                'vision',
                'objetivo'
            )->orderBy('id','desc')->first();

            //----MULTIMEDIA----//
            // ------GALERIA----- //
            $galerias = Galeria::select(
                'id',
                'nombre',
                'slug',
                'published_at'
            )
            ->where('published_at','<', Carbon::now()->format('Y-m-d H:i:s'))
            ->orderBy('published_at','desc')
            ->take(2)
            ->get();

            // ----- VIDEOS ---- //
            $videos = Video::select(
                'id',
                'titulo',
                'url',
                'descripcion'
            )->take(2);


            $audios = Audio::select(
                'id',
                'titulo',
                'archivo',
                'descripcion'
            )->where('activo', 'like', '%si%')->get();

            //----BANNER----//
    		$banners = Banner::where('activo', 'like', '%si%')->get();

            //----RADIO----//
            $programacion = Programas::select(
                'id',
                'titulo',
                'locutor',
                'hora_ini',
                'hora_fin',
                'dias',
                'url'
            )->get();


            $dias = [];
            foreach ($programacion as $key => $a) {
                $_dias = explode(',', $a->dias );
                foreach ($_dias as $key => $dia) {
                    if (!isset($dias[intval($dia)])) {
                        $dias[intval($dia)] = collect();
                    }

                    $dias[intval($dia)]->push([
                        'dias'     =>  intval($dia),
                        'titulo'   =>  $a->titulo,
                        'locutor'  =>  $a->locutor,
                        'hora_ini' =>  $a->hora_ini,
                        'hora_fin' =>  $a->hora_fin,
                        'url'      =>  $a->url
                    ]);
                }
            }

            $_dias = collect($dias);
            $dias = collect();
            foreach ($_dias as $d) {
                $sorted = $d->sortBy('hora_ini');
                $dias->push($sorted->values()->all());
            }

            $audioProgramas = AudioProgramas::select(
                'id',
                'titulo',
                'url'
            ); */

            return $this->view('pagina::index',[
                // 'banners'           => $banners,
                // 'imagenes'          => $imagenes,
                // 'noticias'          => $noticias,
                // 'meses'             => $meses,
                // 'programaciones'    => $dias,
                // 'organizaciones'    => $organizacion,
                // 'videos'            => $videos,
                // 'audios'            => $audios,
                // 'debates'           => $debate,
                // 'efemerides'        => $efemerides,
                // 'eventos'           => $eventos,
                // 'audioProgramas'    => $audioProgramas,
                // 'galerias'          => $galerias
            ]);
        } catch (\Exception $e) {
            return $this->view('pagina::test');
        }


    }
}
