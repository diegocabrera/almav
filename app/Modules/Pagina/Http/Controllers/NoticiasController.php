<?php

namespace App\Modules\Pagina\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Modules\Pagina\Http\Controllers;
use Carbon\Carbon;
use DB;
//MODELOS//
//----NOTICIAS----//
use App\Modules\Noticias\Models\Noticias;
use App\Modules\Noticias\Models\Imagenes;

class NoticiasController extends Controller
{
    public $titulo = 'Noticias RRB';
    public $css = [
        'style',
        'index',
        'noticias',
        'ihover.min.css',
        'mjes',
        'gdlr'
    ];
    public $js = [
        'pagina',
        'move-top',
        'jquery.jplayer.min',
        'circle.player.js',
        'player',
        'noticias',
        'numscroller-1.0'
    ];
    public $meses = [
        1 => "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
    ];

    public function index()
    {

        //----NOTICIAS----//
        $noticias = Noticias::select(
            'id',
            'titulo',
            'resumen',
            'contenido_html',
            'contenido',
            'slug',
            'published_at'
        )->where('published_at','<', Carbon::now()->format('Y-m-d H:i:s'))
        ->orderBy('published_at','desc');

        $mes = [];
        $meses = [];
        foreach ($noticias->get() as $key => $noticia) {
            $mes[] = substr($noticia->published_at, 6, -12);
        }

        $imagenes = Imagenes::select('archivo','leyenda');


        return $this->view('pagina::Noticias',[
            'noticias'  =>  $noticias,
            'imagenes'  =>  $imagenes
        ]);
    }


}
