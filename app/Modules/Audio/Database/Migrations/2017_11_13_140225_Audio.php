<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Audio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('audio', function(Blueprint $table)
         {
             $table->increments('id');
             $table->integer('categorias_id')->unsigned();
             $table->string('archivo', 250);
             $table->string('titulo', 250);
             $table->string('slug', 250)->unique();
             $table->string('descripcion', 250);
             $table->string('activo', 10);

             $table->foreign('categorias_id')->references('id')->on('categorias')->onUpdate('cascade')->onDelete('cascade');

             $table->timestamps();
             $table->softDeletes();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('audio');
     }
}
