<?php

Route::group(['middleware' => 'web', 'prefix' => Config::get('admin.prefix'). '/audio', 'namespace' => 'App\\Modules\Audio\Http\Controllers'], function()
{

    Route::get('/',                        'AudioController@index');
    Route::get('buscar/{id}',              'AudioController@buscar');

    Route::post('guardar',                 'AudioController@guardar');
    Route::put('guardar/{id}',             'AudioController@guardar');

    Route::delete('eliminar/{id}',         'AudioController@eliminar');
    Route::post('restaurar/{id}',          'AudioController@restaurar');
    Route::delete('destruir/{id}',         'AudioController@destruir');

    Route::get('datatable',                'AudioController@datatable');
    Route::post('subir',                   'AudioController@subir');
});
