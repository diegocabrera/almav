var aplicacion, $form, tabla, $archivo_actual = '', $archivos = {}, cordenadasImagen;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.ajax.reload();
			$("#foto").prop("src", "../../public/img/banners/banner.jpg')");
		},
		'buscar' : function(r){
			$('#btn1').click();
			$("#foto").prop("src", r.url);
			console.log(r);
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [
			{data:'titulo',			name:'titulo'},
			{data:'resumen',		name:'resumen'},
			{data:'published_at',	name:'Publicación'}
		]
	});

	$("#upload_link").on('click', function (e) {
		e.preventDefault();
		$("#upload:hidden").trigger('click');
	});

	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});

	$('#tabla').on("click", "tbody tr", function(){
        aplicacion.buscar(this.id);
    });

	$('#published_at', $form).datetimepicker({
		dateFormat: 'yy-mm-dd '
	});
        return false;
});

$(document).ready(function () {
	$("#titulo").keyup(function () {
		var value = slug($("#titulo").val());
		$("#slug").val(value);
	});
});
function dataImagen(cordenadas){
	cordenadasImagen = cordenadas;
}

function stringToJson(str){
	return $.parseJSON(str);
}

function jsonToString(json){
	return JSON.stringify(json);
}
