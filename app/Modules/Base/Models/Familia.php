<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Models\Modelo;

class Familia extends modelo
{
    protected $table = 'familias';
    protected $fillable = ["personas_id","usuarios_id","cabeza"];
    protected $campos = [
        'usuarios_id' => [
            'type'        => 'select',
            'label'       => 'Usuario Familia',
            'placeholder' => '- Seleccione una familia',
            'url'         => 'usuarios'
        ],
        'cabeza' => [
            'type'        	=> 'select',
            'label'       	=> '¿Es Cabeza de Familia?',
            'placeholder' 	=> '-- Seleccione',
			'options' 		=> [
				0	=>	'No',
				1	=>	'Sí'
			]
        ]
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['usuario_id']['options'] = Usuario::pluck('usuario', 'id');
    }
    public function personas()
    {
        return $this->belongsTo('App\Modules\Base\Models\Personas', 'personas_id');
    }

    public function usuarios()
    {
        return $this->belongsTo('App\Modules\Base\Models\Usuario', 'usuarios_id');
    }
}
