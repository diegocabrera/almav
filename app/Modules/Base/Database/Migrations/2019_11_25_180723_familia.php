<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Familia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('familias', function(Blueprint $table){
			$table->increments('id');
			$table->integer('usuarios_id')->unsigned();
			$table->integer('personas_id')->unsigned();
			$table->boolean('cabeza');

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('personas_id')
				->references('id')->on('personas')
				->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('usuarios_id')
				->references('id')->on('app_usuario')
				->onDelete('cascade')->onUpdate('cascade');

		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('familia');
    }
}
