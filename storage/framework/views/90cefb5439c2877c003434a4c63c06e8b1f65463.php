<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <?php echo $__env->make('pagina::partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </head>
    <body>
        <?php echo $__env->make('pagina::partials.header-index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="row">
            <div class="contendor-body">
                <?php echo $__env->yieldContent('content-top'); ?>
                <?php echo $__env->yieldContent('content'); ?>

                <?php echo $__env->make('pagina::partials.footer-page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('pagina::partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>

        </div>


    </body>
</html>
