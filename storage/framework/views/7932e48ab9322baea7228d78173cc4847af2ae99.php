    <!-- service part start-->
    <section class="service_part">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-4 col-sm-10">
                    <div class="service_text">
                        <h2>Apoyamos y ayudamos a los Venezolanos en Logroño</h2>
                        <p>Texto explicativo del tipo de apoyo... </p>
                        
                    </div>
                </div>
                <div class="col-lg-7 col-xl-6">
                    <div class="service_part_iner">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6">
                                <div class="single_service_text ">
                                    <i class="flaticon-money"></i>
                                    <h4>Humanidad</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur elit seiusmod tempor incididunt</p>
                                    
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6">
                                <div class="single_service_text">
                                    <i class="flaticon-money"></i>
                                    <h4>Alimentación</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur elit seiusmod tempor incididunt</p>
                                    <a href="#">contact us</a>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6">
                                <div class="single_service_text">
                                    <i class="flaticon-money"></i>
                                    <h4>Se voluntario</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur elit seiusmod tempor incididunt</p>
                                    <a href="#">read more</a>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6">
                                <div class="single_service_text">
                                    <i class="flaticon-money"></i>
                                    <h4>Ayuda Infantil</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur elit seiusmod tempor incididunt</p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- service part end-->