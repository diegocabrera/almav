<?php $__env->startSection('content'); ?>



<!-- Head& -->
<?php echo $__env->make('pagina::partials.header-index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- Banner -->
<?php echo $__env->make('pagina::partials.banner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- Servicios -->
<?php echo $__env->make('pagina::partials.servicios', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- Eventos -->
<?php echo $__env->make('pagina::partials.eventos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- Nosotros -->
<?php echo $__env->make('pagina::partials.about', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- -->
<!-- -->
<!-- -->




<!-- Footer -->
<?php echo $__env->make('pagina::partials.footer-page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- Realizado por la dirección de Informatica y Sistemas de la Gobernación del Estado Bolívar -->


<!-- Creditos: -->

<!-- Diseño: Maria Campora "camporamaria@gmail.com" -->

<!-- Desarrollador: Alejandro Mendez "alejmendez.87@gmail.com" -->

<!-- Desarrollador: Diego Cabrera "diegocabrera123@gmail.com" -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('pagina::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>